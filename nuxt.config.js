export default {
  mode: 'spa',
  target: 'static',
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || '',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },
  css: [
    '~/assets/styles/utils/index.scss',
    'normalize.css',
    '~/assets/styles/main.scss',
  ],
  plugins: [
    {
      src: '~/plugins/fontAwesome.js',
    },
    {
      src: '~/plugins/vueCarousel.js',
    },
    {
      src: '~/plugins/vueFragment.js',
    },
    {
      src: '~/plugins/vueValidate.js',
    },
  ],
  components: false,
  buildModules: [
    [
      '@nuxtjs/router',
      {
        path: './pages/',
      },
    ],
    '@nuxtjs/eslint-module',
    '@nuxtjs/style-resources',
  ],
  styleResources: {
    scss: ['~/assets/styles/_internal/index.scss'],
  },
  modules: [],
  build: {},
}
