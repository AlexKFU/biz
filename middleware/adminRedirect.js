export default function ({ route, redirect }) {
  // auth imitation
  const {
    query: { auth },
  } = route
  if (+auth) {
    redirect('/admin')
  }
}
