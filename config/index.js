import contacts from './contacts.json'
import links from './links.json'

export default {
  ...contacts,
  ...links,
}
