import Vue from 'vue'
import Router from 'vue-router'

import IndexPage from '@/pages/index'
import RegistrationPage from '@/pages/registration/index'
import AuthPage from '@/pages/auth/index'
import AuthForgotPage from '@/pages/auth/forgot'
import AdminPage from '@/pages/admin/index'
import AdminLayout from '@/layouts/admin'
import SettingsPage from '@/pages/admin/settings'
import RatePage from '@/pages/admin/rate'
import NotFoundError from '@/pages/error/404'

Vue.use(Router)

export function createRouter() {
  return new Router({
    mode: 'history',
    routes: [
      {
        path: '/',
        component: IndexPage,
        meta: {
          crumbs: [
            {
              link: '/',
              name: 'Главная страница',
            },
          ],
        },
      },
      {
        path: '/registration',
        component: RegistrationPage,
        meta: {
          crumbs: [
            {
              link: '/',
              name: 'Главная страница',
            },
            {
              link: '/registration',
              name: 'Регистрация',
            },
          ],
        },
      },
      {
        path: '/auth',
        component: AuthPage,
        meta: {
          crumbs: [
            {
              link: '/',
              name: 'Главная страница',
            },
            {
              link: '/auth',
              name: 'Авторизация',
            },
          ],
        },
        children: [
          {
            path: 'forgot',
            component: AuthForgotPage,
            meta: {
              crumbs: [
                {
                  link: '/',
                  name: 'Главная страница',
                },
                {
                  link: '/auth',
                  name: 'Авторизация',
                },
                {
                  link: '/auth/forgot',
                  name: 'Восстановление пароля',
                },
              ],
            },
          },
        ],
      },
      {
        path: '/admin',
        component: AdminLayout,
        children: [
          {
            path: '',
            component: AdminPage,
            meta: {
              crumbs: [
                {
                  link: '/admin',
                  name: 'Главная страница',
                },
              ],
            },
          },
          {
            path: 'settings',
            component: SettingsPage,
            meta: {
              crumbs: [
                {
                  link: '/admin',
                  name: 'Главная страница',
                },
                {
                  link: '/admin/settings',
                  name: 'Настройки',
                },
              ],
            },
          },
          {
            path: 'rate',
            component: RatePage,
            meta: {
              crumbs: [
                {
                  link: '/admin',
                  name: 'Главная страница',
                },
                {
                  link: '/admin/rate',
                  name: 'Тариф базовый',
                },
              ],
            },
          },
        ],
      },
      {
        path: '/404',
        name: '404',
        component: NotFoundError,
        meta: {
          crumbs: [
            {
              link: '/',
              name: 'Главная страница',
            },
            {
              link: '/404',
              name: 'Страница не найдена',
            },
          ],
        },
      },
      {
        path: '*',
        redirect: '/404',
      },
    ],
  })
}
