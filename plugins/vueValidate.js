import { extend } from 'vee-validate'
import { required, email, is } from 'vee-validate/dist/rules'

const TELEPHONE_REGEX = /(8|\+7)\d{9}/

const PASSPORT_SERIAL_REGEX = /\d{4}/

const PASSPORT_NUMBER_REGEX = /\d{6}/

const PASSPORT_CODE_REGEX = /\d{3}(-)?\d{3}/

const VALIDATORS_OVERRIDE = {
  required: {
    validator: required,
    message: 'Заполните поле',
  },
  email: {
    validator: email,
    message: 'Неправильный email',
  },
  samePass: {
    validator: is,
    message: 'Пароли не совпадают',
  },
  telephone: {
    validator: {
      computesRequired: true,
      validate(value) {
        return TELEPHONE_REGEX.test(value)
      },
    },
    message: 'Введите корректный телефон',
  },
  passportSerial: {
    validator: {
      computesRequired: true,
      validate(value) {
        return PASSPORT_SERIAL_REGEX.test(value)
      },
    },
    message: 'Введите корректную серию паспорта',
  },

  passportNumber: {
    validator: {
      computesRequired: true,
      validate(value) {
        return PASSPORT_NUMBER_REGEX.test(value)
      },
    },
    message: 'Введите корректную номер паспорта',
  },
  passportCode: {
    validator: {
      computesRequired: true,
      validate(value) {
        return PASSPORT_CODE_REGEX.test(value)
      },
    },
    message: 'Введите корректный код подразделения',
  },
}

Object.keys(VALIDATORS_OVERRIDE).forEach((item) => {
  const { validator, message } = VALIDATORS_OVERRIDE[item]
  extend(item, {
    ...validator,
    message,
  })
})
